package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.order.Order;

public class Aims {
	private static final int max_orders = 5;
	private static List<Media> mediaList = new ArrayList<>();
	private List<Order> orders = new ArrayList<>();
	private Order curOrder = null;
	
	public Aims() {
	}
	public static void showItems() {
		mediaList.add(new DigitalVideoDisc("Frozen 2", "Animation", "Jennifer Lee", 86, 29.95f));
		mediaList.add(new DigitalVideoDisc("The Avengers", "Science Fiction", "Joss Whedon", 142, 21.95f));
		mediaList.add(new DigitalVideoDisc("Alladin", "Live action", "Guy Ritchie", 90, 98.99f));
		mediaList.add(new DigitalVideoDisc("Harry aka Potter", "Magic", "Hello World", 150, 49.99f));
		mediaList.add(new Book("Frozen", "Animation", Arrays.asList("Author a", "Author b"), 51.5f));
		mediaList.add(new Book("Avengers", "Science Fiction", Arrays.asList("Author c", "Author b"), 39.5f));
		mediaList.add(new Book("Lilifilm", "Animation", Arrays.asList("Author d", "Author p"), 23.0f));
		mediaList.add(new Book("The hobbit", "Magic", Arrays.asList("Author e", "Author s"), 18.5f));
		mediaList.add(new Book("The lord of the rings", "Fantasy", Arrays.asList("Author f", "Author z"), 102.2f));
		mediaList.forEach(System.out::println);
	}

	public static void showMenu() {
		System.out.println("------------ All items in stock --------------");
		showItems();

		System.out.println("\nOrder Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order (Max: 3 orders)");
		System.out.println("2. Add item to the order (Max: 10 items)");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("5. Buy");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4-5");
		System.out.println("**HOT** Buy 3 get 1 random item for free of charge");
	}
	
	public void createNewOrder() {
		if(this.orders.size()<max_orders) {
			Order order = new Order();
			this.curOrder = order;
			return;
		}
		System.out.println("The number of order is full. Please comeback next time!");
		return;
	}
	
	public void addItemToOrder() {
		int id = 0;
		Scanner scan = new Scanner(System.in);
		
		if (this.curOrder == null) {
			System.out.println("No order created");
//			scan.close();
			return;
		}
		Order order = this.curOrder;
		String type;
		boolean check;

		System.out.println("You want to order Book or Digital Video Disc? ");
		System.out.println("Enter \"book\" or \"disc\" only");
		do {
			System.out.print("your choice: ");
			type = scan.nextLine();
			check = type.toLowerCase().equals("book") || type.toLowerCase().equals("disc");
			if (!check)
				System.out.println("Not valid! Enter again");
		} while (!check);

		System.out.print("Enter title: ");
		String title = scan.nextLine();
		
		type = type.toLowerCase();
		
		switch(type) {
			case "book":
				Optional<Media> book = mediaList.stream().filter(
					item -> item instanceof Book && item.getTitle().toLowerCase().equals(title.toLowerCase()))
					.findFirst();

				if (book.isPresent()) {
					id++;
					Book bookTemp = (Book) book.get();
					bookTemp.setID(id);
					order.addMedia(bookTemp);
					System.out.println("Book " + book.get().getTitle() +" added");
				} else
					System.out.println("No such book found!!");
				break;
				
			case "disc":
				Optional<Media> disc = mediaList.stream().filter(item -> item instanceof DigitalVideoDisc
					&& item.getTitle().toLowerCase().equals(title.toLowerCase())).findFirst();
				if (disc.isPresent()) {
					id++;
					DigitalVideoDisc temp = (DigitalVideoDisc) disc.get();
					temp.setID(id);
					order.addMedia(disc.get());
					System.out.println("Disc " + disc.get().getTitle() +" added");
				} else
					System.out.println("No such disc found!!");
				break;
			default:
				break;
		}
		return;
		
	}
	
	public void removeItems() {
		Scanner scanner1 = new Scanner(System.in);
		
		if (this.curOrder==null) {
			System.out.println("No order created");
//			scanner1.close();
			return;
		}
		Order order = this.curOrder;
		System.out.print("Enter id to delete: ");
		int deleteId = Integer.parseInt(scanner1.nextLine());
		
		List<Media> list = order.getItemsOrdered().stream().filter(item -> item.getID() != deleteId)
				.collect(Collectors.toList());
		
		if (list.size() == order.getItemsOrdered().size())
			System.out.println("Delete failed! No such id");
		order.setItemsOrdered(list);
//		scanner1.close();
		return;
	}
	
	public void printOrder() {
		if (this.curOrder==null) {
			System.out.println("No order created");
			return;
		}
		Order order = this.curOrder;
		order.printOrder();
		if (order.getItemsOrdered().size() > 5)
			order.printOrd_W_LuckyItem();
	}
	
	public static void main(String[] args) {
		int choice = 0;
		Scanner scanner = new Scanner(System.in);
		Aims aims = new Aims();
		boolean exit = false;

		showMenu();
		while (true){
			
			System.out.print("\nInput a choice: ");
			choice = Integer.parseInt(scanner.nextLine());

			switch (choice) {
			case 1:
				aims.createNewOrder();
				break;
			case 2:
				aims.addItemToOrder();
				break;

			case 3:
				aims.removeItems();
				break;

			case 4:
				aims.printOrder();
				break;
				
			case 5:
				aims.orders.add(aims.curOrder);
				aims.curOrder = null;
				System.out.println("The order is finished. Please create a new order to continue buying.");
				break;

			case 0:
				System.out.println("Exit!");
				exit = true;
				break;

			default:
				System.out.println("Invalid choice, try again!");
				break;
			}
			if(exit==true) break;
		} 
		scanner.close();
	}

}
