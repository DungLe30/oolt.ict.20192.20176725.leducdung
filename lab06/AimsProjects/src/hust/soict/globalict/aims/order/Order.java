package hust.soict.globalict.aims.order;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Book;

import java.util.Date;


public class Order {
	public static final int MAX_ITEMS_ORDERED = 10;
	public static final int MAX_LIMIT_ORDERED = 5;

//	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
	private List<Media> itemsOrdered = new ArrayList<Media>();
	private int qtyOrdered;
	private Date dateOrdered;
	private int nbOrdered;

	public Order() {
		this.setNbOrdered();
		if(++this.nbOrdered<MAX_LIMIT_ORDERED){
			this.dateOrdered = new Date();
			System.out.println("Order "+this.nbOrdered+" created.");
		} else {
			System.out.println("Cannot make new order. Order limit reached.");
			System.exit(0);
		}
	}

	public int getNbOrdered() {
		return this.nbOrdered;
	}

	public void setNbOrdered() {
		this.nbOrdered = 0;
	}

	public Date getDateOrdered() {
		return this.dateOrdered;
	}

	public void setDateOrdered(Date dateOrd) {
		this.dateOrdered = dateOrd;
	}

	public int getQtyOrdered() {
		return this.qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public List<Media> getItemsOrdered() {
		return this.itemsOrdered;
	}
	
	public void setItemsOrdered(List<Media> ItemsList) {
		this.itemsOrdered = ItemsList;
	}

	// Add distincr=t order
	public void addMedia(Media media) {
		if(this.itemsOrdered.size()<MAX_ITEMS_ORDERED) {
			this.itemsOrdered.add(media);
			return;
		}
		System.out.println("The order's limit reached");
		return;
	}
	

	// Add a list of ordered
	public void addMedia (List<Media> mediaList) {
		int size = mediaList.size();
		if(this.itemsOrdered.size()+size<=MAX_ITEMS_ORDERED) {
			this.itemsOrdered.addAll(mediaList);
			return;
		}
		System.out.println("The order's limit reached");
		return;
	}

	// Add double at once
	public void addMedia(Media media1, Media media2) {
		int gap = MAX_ITEMS_ORDERED-this.itemsOrdered.size();
		if(gap<=2){
			this.itemsOrdered.add(media1);
			this.itemsOrdered.add(media2);
			this.qtyOrdered+=2;
		}
		else if(gap==1){
			System.out.println("One item annot be add");
		} else System.out.println("The order is full");
	}

	// Remove existed ordered
	public void removeMedia(Media media) {
		if (itemsOrdered.contains(media)) {
			if (itemsOrdered.size() != 0) {
				itemsOrdered = itemsOrdered.stream().filter(m -> m != media).collect(Collectors.toList());
				System.out.format("The media \"%s\" has been removed\n", media.getTitle());
			} else {
				System.out.println("Order is empty. Please add!");
			}
		} else {
			System.out.println("Your order doesnt contain " + media.getTitle());
		}
	}

	// Return total cost of order
	public double totalCost() {
		return itemsOrdered.stream().mapToDouble(m -> m.getCost()).sum();
	}
	
	public Media getLuckyItem() {
		int size = this.itemsOrdered.size();
		int itemIndex = (int) (Math.random()*size);
		Media Item = itemsOrdered.get(itemIndex);
		Item.setCost(0);
		return Item;
	}

	public void printOrder() {
		System.out.println("++++++++ ORDER LIST +++++++++");
		System.out.println("Date: "+this.dateOrdered);
		itemsOrdered.forEach(item -> {
			if (item instanceof DigitalVideoDisc) {
				System.out.println(item.getID() + ". " + item.getTitle() + " - " + item.getCategory() + " - "
						+ ((DigitalVideoDisc) item).getDirectory() + " - " + ((DigitalVideoDisc) item).getLength()
						+ ": $" + item.getCost());
			}
			if (item instanceof Book)
				System.out.println(item.getID() + ". " + item.getTitle() + " - " + item.getCategory() + " - "
						+ ((Book) item).getAuthors() + ": $" + item.getCost());
		});
		System.out.print("\nTotal Cost: "+totalCost()+"\n");
		System.out.println("++++++++++++++++++++++++++++++");
	}
	
	public void printOrd_W_LuckyItem() {
		System.out.println("************************* ORDER WITH LUCKY ITEM ************************");
		System.out.println("Date: " + getDateOrdered());
		System.out.println("Ordered items: ");
		for (int i = 0; i < itemsOrdered.size(); i++) {
			System.out.println((i + 1) + ". " + itemsOrdered.get(i).getTitle() + " - "
					+ itemsOrdered.get(i).getCategory() + " - " + ": $" + itemsOrdered.get(i).getCost());
		}
		Media lucky = getLuckyItem();
		System.out.println("The lucky item is: "+lucky.getTitle()+" "+lucky.getCategory()+" "+lucky.getCost());
		
		System.out.print("\nTotal Cost is: "+totalCost()+"\n");
		System.out.println("++++++++++++++++++++++++++++++");
	}
	
}
