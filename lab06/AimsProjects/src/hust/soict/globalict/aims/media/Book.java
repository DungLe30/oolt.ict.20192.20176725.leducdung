package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Book extends Media {
	private List<String> authors = new ArrayList<String>();
	public Book() {
		// TODO Auto-generated constructor stub
	}
	


	public Book(String title, String category) {
		super(title, category);
	}

	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
		// TODO: check author condition
	}

	public Book(String title, String category, List<String> authors, float cost) {
		super(title, category, cost);
		this.authors = authors;
		// TODO: check author condition
	}

	public Book(int id, String title, String category, float cost, List<String> authors) {
		super(id, title, category, cost);
		this.authors = authors;
		// TODO: check author condition
	}
	
	public String toString() {
		return "Book [title= " + getTitle() + ", cost = $" + getCost() + "]";
	}
	
	public void addAuthor(String authorName) {
		String name = authorName.trim();
		if(authors.indexOf(name)!=-1) {
			System.out.println("THe author is already existed in the list.");
			return;
		}
		
		authors.add(name);
		return;
	}
	
	public List<String> getAuthors() {
		return this.authors;
	}
	
	public void removeAuthor(String authorName) {
		String name = authorName.trim();
		int index = authors.indexOf(name);
		if(index==-1) {
			System.out.println("THe author is not existed in the list.");
			return;
		}
		
		authors.remove(index);
		return;
	}
}
