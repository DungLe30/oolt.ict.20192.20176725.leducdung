package hust.soict.globalict.aims.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Scanner;

public class MyDate {
	private int day;
	private int month;
	private int year;
	private String dayStr;
	private String monStr;
	private String yearStr;
	
	private String getDaySuffix(int day) {
		if (day >= 11 && day <= 13) {
			return "th";
		}
		switch (day % 10) {
		case 1:
			return "st";
		case 2:
			return "nd";
		case 3:
			return "rd";
		default:
			return "th";
		}
	}
	
	//Convert String to LocalDate (including check date format) 
	private LocalDate convertToLocalDate(String strDate) {
		int index[] = new int[4];
		if (strDate.contains("th") || strDate.contains("st") || strDate.contains("rd") || strDate.contains("nd")) {
			index[0] = strDate.indexOf("th");
			index[1] = strDate.indexOf("st");
			index[2] = strDate.indexOf("nd");
			index[3] = strDate.indexOf("rd");
			for (int i = 0; i < index.length; i++) {
				if (index[i] > 0) {
					int day = Integer.parseInt(strDate.substring(index[i] - 2, index[i]).trim());
					if (i == 0) {
						String temp = getDaySuffix(day);
						if (temp != "th")
							return null;
						strDate = strDate.replace("th", "");
					} else if (i == 1) {
						String temp = getDaySuffix(day);
						if (temp != "st")
							return null;
						strDate = strDate.replace("st", "");
					} else if (i == 2) {
						String temp = getDaySuffix(day);
						if (temp != "nd")
							return null;
						strDate = strDate.replace("nd", "");
					} else if (i == 3) {
						String temp = getDaySuffix(day);
						if (temp != "rd")
							return null;
						strDate = strDate.replace("rd", "");
					}
					break;
				}
			}
		}

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM dd yyyy");
		Date date = new Date();
		try {
			date = simpleDateFormat.parse(strDate);
			return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		} catch (ParseException e) {
			return null;
		}
	}

	public MyDate() {

	}

	public MyDate(String strDate) {
		LocalDate localDate = convertToLocalDate(strDate);
		if (localDate == null) {
			System.out.println("Input Date Error");
			return;
		}

		System.out.println(localDate.toString());
		int year = localDate.getYear();
		int month = localDate.getMonthValue();
		int day = localDate.getDayOfMonth();
		this.setDay(day);
		this.setMonth(month);
		this.setYear(year);
	}

	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public void setDay(String Day) {
		this.dayStr = Day;
	}

	public void setMonth(String Mon) {
		this.monStr = Mon;
	}

	public void setYear(String Year) {
		this.yearStr = Year;
	}

	public void accept() {
		Scanner scan = new Scanner(System.in);
		System.out.print("Input a date: ");
		String strDate = scan.nextLine();
		LocalDate localDate = convertToLocalDate(strDate);
		if (localDate == null) {
			System.out.println("Input Date Error");
			scan.close();
			return;
		}

		System.out.println(localDate.toString());
		int year = localDate.getYear();
		int month = localDate.getMonthValue();
		int day = localDate.getDayOfMonth();
		this.setDay(day);
		this.setMonth(month);
		this.setYear(year);
		
		scan.close();
	}

	public String monthToStr(int month) {
		String monthName[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		String monthWord = "";
		monthWord+= monthName[month-1];
		return monthWord;
	}

	public String yearToStr(int input){
		int number = input;
		String unitCount[] = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", 
								"eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
		String unitPlace[] = {"zero", "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"
						, "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fouteenth", "fifteenth", 
						"sixteenth", "seventeenth", "eighteenth", "ninteenth"};
		String tenMulti[] = {"zero", "ten", "twenty", "thirty", "fifty", "sixty", "seventy", "eighty", "ninety"};
		String num = Integer.toString(input);
		String words = "";
		
		if ((number / 1000) > 0) {
			words += yearToStr(number / 1000) + " thousand ";
			number %= 1000;
		}
		// check if number is divisible by 1 hundred
		if ((number / 100) > 0) {
			 words += yearToStr(number / 100) + " hundred ";
			 number %= 100;
		}
	 
		if (number > 0) {
			 // check if number is within teens
			 if (number < 20) { 
						// fetch the appropriate value from unit array
						words += unitCount[number];
				 } else { 
					// fetch the appropriate value from tens array
					words += tenMulti[number / 10]; 
					if ((number % 10) > 0) {
				words += "-" + unitCount[number % 10];
					}  
			 }
			  }
		  return words;
	}

	public String dayToStr(int input){
		int number = input;
		String unitPlace[] = {"zero", "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"
						, "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fouteenth", "fifteenth", 
						"sixteenth", "seventeenth", "eighteenth", "ninteenth"};
		String tenMulti[] = {"zero", "ten", "twenty", "thirty", "fifty", "sixty", "seventy", "eighty", "ninety"};
		String num = Integer.toString(input);
		String words = "";
		
		if ((number / 1000) > 0) {
			words += dayToStr(number / 1000) + " thousand ";
			number %= 1000;
		}
		// check if number is divisible by 1 hundred
		if ((number / 100) > 0) {
			 words += dayToStr(number / 100) + " hundred ";
			 number %= 100;
		}
	 
		if (number > 0) {
			 // check if number is within teens
			 if (number < 20) { 
						// fetch the appropriate value from unit array
						words += unitPlace[number];
				 } else { 
					// fetch the appropriate value from tens array
					words += tenMulti[number / 10]; 
					if ((number % 10) > 0) {
				words += "-" + unitPlace[number % 10];
					}  
			 }
			  }
		  return words;
	}


	public void acceptStr() {
		Scanner scan = new Scanner(System.in);
		System.out.print("Input a date: ");
		String strDate = scan.nextLine();
		LocalDate localDate = convertToLocalDate(strDate);
		if (localDate == null) {
			System.out.println("Input Date Error");
			scan.close();
			return;
		}

		System.out.println(localDate.toString());
		int day = localDate.getDayOfMonth();
		int month = localDate.getMonthValue();
		int year = localDate.getYear();

		String dayStr = dayToStr(day);
		String monStr = monthToStr(month);
		String yearStr = yearToStr(year);

		this.setDay(dayStr);
		this.setMonth(monStr);
		this.setYear(yearStr);
		
		scan.close();
	}

	public void print() {
		System.out.format("Day: %d\nMonth: %d\nYear: %d\n", this.getDay(), this.getMonth(), this.getYear());
	}

	public void printStr() {
		System.out.format("%s of %s %s \n", this.dayStr, this.monStr, this.yearStr);
	}
}
