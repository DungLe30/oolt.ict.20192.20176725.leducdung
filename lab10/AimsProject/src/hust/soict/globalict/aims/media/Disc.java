package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Playable;
import hust.soict.globalict.aims.PlayerException;

public class Disc extends Media implements Playable, Comparable{
	
	private int length;
	private String directory;
	
	public void setLength(int length) {
		this.length = length;
	}
	
	public int getLength() {
		return this.length;
	}
	
	public void setDirectory(String director) {
		this.directory = director;
	}
	
	public String getDirectory() {
		return this.directory;
	}
	public Disc() {
		// TODO Auto-generated constructor stub
	}

	
	public Disc(String title) {
		super(title);
	}

	public Disc(String title, String category) {
		super(title, category);
	}

	public Disc(String title, String category, String directory) {
		super(title, category);
		this.directory = directory;
	}
	
	public Disc(String title, String category, float cost) {
		super(title, category, cost);
	}
	
	
	public Disc(String title, String category, String directory, int length) {
		super(title, category);
		this.directory = directory;
		this.length = length;
	}
	
	public Disc(String title, String category, String directory, float cost) {
		super(title, category, cost);
		this.directory = directory;
	}
	

	public Disc(String title, String category, String directory, int length, float cost) {
		super(title, category, cost);
		this.directory = directory;
		this.length = length;
	}

	
	public Disc(int id, String title, float cost) {
		super(id, title, cost);
	}
	
	public Disc(int id, String title, String category, String directory, float cost) {
		super(id, title, category, cost);
		this.directory = directory;
	}
	
	
	public Disc(int id, String title, String category, String directory, int length, float cost) {
		super(id, title, category, cost);
		this.directory = directory;
		this.length = length;
	}
	
	public int compareTo(Object obj) {
		return 0;
	}
	
	public void play() throws PlayerException {}

}
