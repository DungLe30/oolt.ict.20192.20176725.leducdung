package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.stream.Collectors;

public class Book extends Media implements Comparable {
	private List<String> authors = new ArrayList<String>();
	
	private String content;
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrequency;
	public String getContent() {
		return content;
	}



	public void setContent(String content) {
		this.content = content;
		this.getTokensAndFrequency();
	}



	public List<String> getContentTokens() {
		return contentTokens;
	}



	public void setContentTokens(List<String> contentTokens) {
		this.contentTokens = contentTokens;
	}



	public Map<String, Integer> getWordFrequency() {
		return wordFrequency;
	}

	public void setWordFrequency(Map<String, Integer> wordFrequency) {
		this.wordFrequency = wordFrequency;
	}

	
	public Book() {
		// TODO Auto-generated constructor stub
	}
	


	public Book(String title, String category) {
		super(title, category);
	}
	
	public Book(String title, String category, float cost) {
		super(title, category, cost);
	}

	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
		// TODO: check author condition
	}

	public Book(String title, String category, List<String> authors, float cost) {
		super(title, category, cost);
		this.authors = authors;
		// TODO: check author condition
	}

	public Book(int id, String title, String category, float cost, List<String> authors) {
		super(id, title, category, cost);
		this.authors = authors;
		// TODO: check author condition
	}
	
	public String toString() {
		return "Book: title= " + getTitle() + ", cost = $" + getCost() + ", contentLength = " + getContentTokens().size()
				+ "\nToken list: " + getContentTokens() + 
				"\nWord Frequency: " + getWordFrequency();
	}
	
	public void addAuthor(String authorName) {
		String name = authorName.trim();
		if(authors.indexOf(name)!=-1) {
			System.out.println("THe author is already existed in the list.");
			return;
		}
		
		authors.add(name);
		return;
	}
	
	public List<String> getAuthors() {
		return this.authors;
	}
	
	public void removeAuthor(String authorName) {
		String name = authorName.trim();
		int index = authors.indexOf(name);
		if(index==-1) {
			System.out.println("THe author is not existed in the list.");
			return;
		}
		
		authors.remove(index);
		return;
	}
	
	public int compareTo(Object obj) {
		Book b = (Book) obj;
		if(this.getCost() == b.getCost())
			return 0;
		return this.getCost()>b.getCost() ? 1 : -1;
	}
	
	public void getTokensAndFrequency() {
		String[] words = this.getContent().split("\\s+");
		List<String> tokens = Arrays.asList(words).stream().map(w -> w.replaceAll("[^\\w]", "")).collect(Collectors.toList());
		tokens.sort(String::compareToIgnoreCase);
		
		this.setContentTokens(tokens);
		
		Map<String, Integer> frequency = new HashMap<>();
		tokens.forEach(t -> frequency.put(t, Collections.frequency(tokens, t)));
		this.setWordFrequency(frequency);
	}
	
}
