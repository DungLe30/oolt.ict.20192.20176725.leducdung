// package AimsProject;

public class Aims {
    public static void main(String[] args) {
		Order anOrder = new Order();

		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 86, 19.95f);
		anOrder.addDigitalVideoDisc(dvd1);

		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124, 24.95f);
		anOrder.addDigitalVideoDisc(dvd2);

		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Alladin", "Animation", "John Musker", 90, 18.99f);
		anOrder.addDigitalVideoDisc(dvd3);
		anOrder.addDigitalVideoDisc(dvd3);
		anOrder.removeDigitalVideoDisc(dvd3);
		anOrder.addDigitalVideoDisc(dvd3);

		System.out.println("Total orders: " + anOrder.getQtyOrdered());
        System.out.format("Total Cost is: %.2f$\n", anOrder.totalCost());
    }
}