package hust.soict.globalict.test;

import hust.soict.globalict.aims.media.Book;

public class TestBookContent {

	public TestBookContent() {
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		Book book = new Book("My Book", "Fantasy", 14.5f);
		book.setContent("This is a a book book book. This book contains contains some content, content.\nThe book is written by by author What, author What, author Who.");
		System.out.println(book.toString());
	}
}
