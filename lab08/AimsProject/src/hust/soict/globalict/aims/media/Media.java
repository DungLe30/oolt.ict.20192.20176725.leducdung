package hust.soict.globalict.aims.media;

public abstract class Media {
	private int ID;
	private String title;
	private String category;
	private float cost;
	
	public int getID() {
		return this.ID;
	}
	
	public void setID(int ID) {
		this.ID = ID;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public Media() {
		// TODO Auto-generated constructor stub
	}
	
	public Media(String title) {
		this.title = title;
	}
	
	public Media(String title, String category) {
		this.title = title;
		this.category = category;
	}

	public Media(String title, String category, float cost) {
		this.title = title;
		this.category = category;
		this.cost = cost;
	}

	public Media(int id, String title, String category, float cost) {
		this.ID = id;
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	
	public Media(int id, String title, float cost) {
		this.ID = id;
		this.title = title;
		this.cost = cost;
	}

}
