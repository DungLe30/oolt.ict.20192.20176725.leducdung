package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.media.Disc;
import hust.soict.globalict.aims.media.Playable;

public class DigitalVideoDisc extends Disc implements Playable, Comparable {
	public DigitalVideoDisc() {
	}

	public DigitalVideoDisc(String title) {
		super(title);
	}

	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}

	public DigitalVideoDisc(String title, String category, String directory) {
		super(title, category, directory);
	}

	public DigitalVideoDisc(String title, String category, String directory, int length, float cost) {
		super(title, category, directory, length, cost);
	}
	
	public DigitalVideoDisc(int id, String title, String category, String directory, int length, float cost) {
		super(id, title, category, directory, length, cost);
	}
	@Override
	public String toString() {
		return "DigitalVideoDisc [title=" + getTitle() + ", category=" + getCategory() + ", directory=" + getDirectory() + ", length="
				+ getLength() + ", cost=" + getCost() + "]";
	}
	
	public boolean search(String title) {
		title = title.toLowerCase();
		String[] keys = title.split(" ");
		String discName = getTitle();
		discName = discName.toLowerCase();		
		for (int i = 0; i < keys.length; i++) {
			if (discName.indexOf(keys[i]) == -1)
				return false;
		}
		return true;
	}
	
	public void play() {
		System.out.println("Now playing DVD: "+this.getTitle());
		System.out.println("DVD length: "+this.getLength());
	}
	
//	Override
	public int compareTo(Object obj) {
		int status = 0;
		DigitalVideoDisc dvd = (DigitalVideoDisc) obj;
		status = this.getTitle().compareTo(dvd.getTitle());
		return status;
	}

}
