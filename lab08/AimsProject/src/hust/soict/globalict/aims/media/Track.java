package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.media.Playable;

public class Track implements Playable {
	
	private String title;
	private int length;
	

	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public int getLength() {
		return length;
	}


	public void setLength(int length) {
		this.length = length;
	}
	
	

	public Track() {
		// TODO Auto-generated constructor stub
	}
	
	public Track(String title, int length) {
		this.length = length;
		this.title = title;
	}
	
	public void play() {
		System.out.println("Now playing track: "+this.getTitle());
		System.out.println("Track length: "+this.getLength());
	}

}
