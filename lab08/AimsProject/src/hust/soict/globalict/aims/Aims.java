package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.Collections;

//import hust.soict.globalict.aims.media.Media;
//import hust.soict.globalict.aims.media.Disc;
//import hust.soict.globalict.aims.media.CompactDisc;
//import hust.soict.globalict.aims.media.DigitalVideoDisc;
//import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.*;


import hust.soict.globalict.aims.order.Order;

public class Aims {
	private static final int max_orders = 5;
	private static List<Media> mediaList = new ArrayList<>();
	private List<Order> orders = new ArrayList<>();
	private Order curOrder = null;
	
	public Aims() {
	}
	public static void showItems() {
		mediaList.add(new DigitalVideoDisc("Frozen 2", "Animation", "Jennifer Lee", 86, 29.95f));
		mediaList.add(new DigitalVideoDisc("The Avengers", "Science Fiction", "Joss Whedon", 142, 21.95f));
		mediaList.add(new DigitalVideoDisc("Alladin", "Live action", "Guy Ritchie", 90, 98.99f));
		mediaList.add(new DigitalVideoDisc("Harry aka Potter", "Magic", "Hello World", 150, 49.99f));
		mediaList.add(new Book("Frozen", "Animation", Arrays.asList("Author a", "Author b"), 51.5f));
		mediaList.add(new Book("Avengers", "Science Fiction", Arrays.asList("Author c", "Author b"), 39.5f));
		mediaList.add(new Book("Lilifilm", "Animation", Arrays.asList("Author d", "Author p"), 23.0f));
		mediaList.add(new Book("The hobbit", "Magic", Arrays.asList("Author e", "Author s"), 18.5f));
		mediaList.add(new Book("The lord of the rings", "Fantasy", Arrays.asList("Author f", "Author z"), 102.2f));
		mediaList.forEach(System.out::println);
	}

	public static void showMenu() {
		System.out.println("------------ All items in stock --------------");
		showItems();

		System.out.println("\nOrder Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order (Max: 3 orders)");
		System.out.println("2. Add item to the order (Max: 10 items)");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("5. Buy");
		System.out.println("6. Test Compare To method");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4-5-6");
		System.out.println("**HOT** Buy 3 get 1 random item for free of charge");
	}
	
	public void createNewOrder() {
		if(this.orders.size()<max_orders) {
			Order order = new Order();
			this.curOrder = order;
			return;
		}
		System.out.println("The number of order is full. Please comeback next time!");
		return;
	}
	
//	Add Cd function
	public void createCD(int id, Order order) {
		Scanner scanner = new Scanner(System.in);
		
		float trackCost = 4.5f;

		id++;
		System.out.print("Enter disc title: ");
		String title = scanner.nextLine();

		System.out.print("Enter artist: ");
		String artist = scanner.nextLine();

		List<Track> tracks = new ArrayList<>();

		System.out.print("Enter number of track (each track is $4.5): ");
		int n = Integer.parseInt(scanner.nextLine()); // number of track

		System.out.println("Enter tracks' infomation: ");
		for (int i = 0; i < n; i++) {
			System.out.println("- Track " + (i + 1));
			
			// Track title
			System.out.print("Enter track's title: ");
			String trackTitle = scanner.nextLine();
			
			// Track length
			System.out.print("Enter track's length: ");
			int length = Integer.parseInt(scanner.nextLine());
			tracks.add(new Track(trackTitle, length)); // add track to list
		}

		CompactDisc disc = new CompactDisc(id, title, artist, tracks, trackCost * tracks.size()); // init disc
		int prevSize = order.getItemsOrdered().size();
		order.addMedia(disc);
		if (prevSize != order.getItemsOrdered().size())
			if (disc.getLength() > 0)
				playDiscOption(disc);
			else 
				System.out.println("No track found");
		
		System.out.println("Disc " + disc.getTitle() + " added\n------");
		
		
		
	}
	
//	Add book function
	public void addBook(int id, Order order) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter title: ");
		String title = scan.nextLine();
		
		Optional<Media> book = mediaList.stream().filter(
			item -> item instanceof Book && item.getTitle().toLowerCase().equals(title.toLowerCase()))
			.findFirst();

		if (book.isPresent()) {
			id++;
			Book bookTemp = (Book) book.get();
			bookTemp = new Book(id, bookTemp.getTitle(), bookTemp.getCategory(), bookTemp.getCost(), bookTemp.getAuthors());
			bookTemp.setID(id);
			order.addMedia(bookTemp);
			System.out.println("Book " + book.get().getTitle() +" added");
		} else
			System.out.println("No such book found!!");
	}
	
//	Add dvd function
	public void addDVD(int id, Order order) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter title: ");
		String title = scan.nextLine();
		
		Optional<Media> disc = mediaList.stream().filter(item -> item instanceof DigitalVideoDisc
			&& item.getTitle().toLowerCase().equals(title.toLowerCase())).findFirst();
		if (disc.isPresent()) {
			id++;
			DigitalVideoDisc temp = (DigitalVideoDisc) disc.get();
			temp = new DigitalVideoDisc(id, temp.getTitle(), temp.getCategory(), temp.getDirectory(), temp.getLength(),
					temp.getCost());
			temp.setID(id);
			int prevSize = order.getItemsOrdered().size();
			
			order.addMedia(temp);
			
			System.out.println("Disc " + disc.get().getTitle() +" added");
			
			if (prevSize != order.getItemsOrdered().size())
				playDiscOption(temp);
		} else
			System.out.println("No such disc found!!");
	}
	
//	Play option
	private static void playDiscOption(Disc disc) {
		Scanner scanner = new Scanner(System.in);
		// TODO Auto-generated method stub
		System.out.println("Do you want to play the selected disc?");
		String choice;
		boolean check;
		do {
			System.out.print("Yes/No -> ");
			choice = scanner.nextLine();
			check = choice.toLowerCase().equals("yes") || choice.toLowerCase().equals("no");
			if (!check)
				System.out.println("Not valid!");
		} while (!check);

		if (choice.toLowerCase().equals("yes")) {
			System.out.println("------");
			disc.play();
		}
	}
	
	
//	Additem to created order
	public void addItemToOrder() {
		if (this.curOrder == null) {
			System.out.println("No order created");
//			scan.close();
			return;
		}
		
		List<Media> curItemsList = this.curOrder.getItemsOrdered();
		int id = curItemsList.size();
		Scanner scan = new Scanner(System.in);
		
		Order order = this.curOrder;
		String type;
		boolean check;

		System.out.println("You want to order Book or Digital Video Disc? ");
		System.out.println("Enter \"book\" or \"disc\" or \"CD\" only");
		do {
			System.out.print("your choice: ");
			type = scan.nextLine();
			check = type.toLowerCase().equals("book") || type.toLowerCase().equals("disc") || type.toLowerCase().equals("cd");
			if (!check)
				System.out.println("Not valid! Enter again");
		} while (!check);
		type = type.toLowerCase();
		
		switch(type) {
			case "book":
				addBook(id, order);
				break;
				
			case "disc":
				addDVD(id, order);
				break;
			case "cd":
				createCD(id, order);
				break;
			default:
				break;
		}
		return;
		
	}
	
	
//	Remove item from created order
	public void removeItems() {
		Scanner scanner1 = new Scanner(System.in);
		
		if (this.curOrder==null) {
			System.out.println("No order created");
//			scanner1.close();
			return;
		}
		Order order = this.curOrder;
		System.out.print("Enter id to delete: ");
		int deleteId = Integer.parseInt(scanner1.nextLine());
		
		List<Media> list = order.getItemsOrdered().stream().filter(item -> item.getID() != deleteId)
				.collect(Collectors.toList());
		
		if (list.size() == order.getItemsOrdered().size())
			System.out.println("Delete failed! No such id");
		order.setItemsOrdered(list);
//		scanner1.close();
		return;
	}
	
//	Test compareTo Method 
	public void testCompareTo() {
		List<DigitalVideoDisc> list = new ArrayList<>();
		list.add(new DigitalVideoDisc("Frozen 2", "Animation", "Jennifer Lee", 86, 29.95f));
		list.add(new DigitalVideoDisc("The Avengers", "Science Fiction", "Joss Whedon", 142, 21.95f));
		list.add(new DigitalVideoDisc("Alladin", "Live action", "Guy Ritchie", 90, 98.99f));
		list.add(new DigitalVideoDisc("Harry Potter", "Magic", "Hello World", 150, 49.99f));
		
		System.out.println("The DVDs currently in the order are:");
		list.forEach(i -> System.out.println(((Media) i).getTitle() + "- $" + ((Media) i).getCost()));
		
		// Sort function		
		Collections.sort(list);
		System.out.println("----------------\nThe DVDs in sorted order are: ");
		list.forEach(i -> System.out.println(((Media) i).getTitle() + "- $" + ((Media) i).getCost()));

	}
	
	public void printOrder() {
		if (this.curOrder==null) {
			System.out.println("No order created");
			return;
		}
		Order order = this.curOrder;
		order.printOrder();
		if (order.getItemsOrdered().size() > 5)
			order.printOrd_W_LuckyItem();
	}
	
	public static void main(String[] args) {
		
		Thread thread = new Thread(new MemoryDaemon());
		thread.setDaemon(true);
		thread.start();
		
		int choice = 0;
		String input;
		Scanner scanner = new Scanner(System.in);
		Aims aims = new Aims();
		boolean exit = false;

		showMenu();
		while (true){
			
			System.out.print("\nInput a choice: ");
			input = scanner.nextLine();
			choice = Integer.parseInt(input);

			switch (choice) {
			case 1:
				aims.createNewOrder();
				break;
			case 2:
				aims.addItemToOrder();
				break;

			case 3:
				aims.removeItems();
				break;

			case 4:
				aims.printOrder();
				break;
				
			case 5:
				aims.orders.add(aims.curOrder);
				aims.curOrder = null;
				System.out.println("The order is finished. Please create a new order to continue buying.");
				break;
			
			case 6:
				aims.testCompareTo();
				break;

			case 0:
				System.out.println("Exit!");
				exit = true;
				break;

			default:
				System.out.println("Invalid choice, try again!");
				break;
			}
			if(exit==true) break;
		} 
		scanner.close();
	}

}
