package hust.soict.globalict.aims;

public class MemoryDaemon implements Runnable {

	public MemoryDaemon() {
		// TODO Auto-generated constructor stub
	}
	
	private long memoryUsed;
	public void run() {
		Runtime runtime = Runtime.getRuntime();
		
		long used;
		
		while(true) {
			used = runtime.totalMemory() - runtime.freeMemory();
			if (used != memoryUsed) {
				System.out.println("Memory Used = " + used);
				memoryUsed = used;
			}
		}
	}

}
