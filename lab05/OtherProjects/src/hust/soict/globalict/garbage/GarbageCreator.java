package hust.soict.globalict.garbage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class GarbageCreator {
	public static void main(String[] args) throws IOException {
		int limit = 100;
		File file = new File("/home/dungle/Desktop/JavaLab/lab05/OtherProjects/src/hust/soict/globalict/garbage/text.txt");
		if (!file.exists() && !file.canRead()) {
			System.out.println("Cant read file");
			return;
		}

		BufferedReader input = new BufferedReader(new FileReader(file));
		String trash = "";
		int prevAdd = trash.hashCode();
		String line;
		int count = 0;
		
		System.out.println(prevAdd);
		
		while ((line = input.readLine()) != null) {
			trash += line;
			trash += "\n";
			
			int curAdd = trash.hashCode();
			if (prevAdd != curAdd) {
				prevAdd = curAdd;
//				System.out.println(curAdd);
				count += 1;
			}
			if(count>limit) {
				System.out.println("The garbage is exceed limit.");
				return;
			}
		}
		System.out.println("Garbage: " + count);
		input.close();
	}
}