package hust.soict.globalict.aims.order;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Date;

import hust.soict.globalict.aims.disc.DigitalVideoDisc;


public class Order {
	public static final int MAX_NUMBER_ORDERED = 10;
	public static final int MAX_LIMIT_ORDERED = 5;

//	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
	private List<DigitalVideoDisc> itemsOrdered = new ArrayList<>();
	private int qtyOrdered;
	private Date dateOrdered;
	private int nbOrdered;

	public Order() {
		this.setNbOrdered();
		if(++this.nbOrdered<MAX_LIMIT_ORDERED){
			this.dateOrdered = new Date();
			System.out.println("Order "+this.nbOrdered+" created.");
		} else {
			System.out.println("Cannot make new order. Order limit reached.");
			System.exit(0);
		}
	}

	public int getNbOrdered() {
		return this.nbOrdered;
	}

	public void setNbOrdered() {
		this.nbOrdered = 0;
	}

	public Date getDateOrdered() {
		return this.dateOrdered;
	}

	public void setDateOrdered(Date dateOrd) {
		this.dateOrdered = dateOrd;
	}

	public int getQtyOrdered() {
		return this.qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}

	// Add distincr=t order
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered < MAX_NUMBER_ORDERED) {
			itemsOrdered.add(disc);
			qtyOrdered = itemsOrdered.size();
			System.out.format("The disc \"%s\" has been added\n", disc.getTitle());
		} else {
			System.out.println("The order is full");
		}
	}


	// Add a list of ordered
	public void addDigitalVideoDisc (List<DigitalVideoDisc> DVDlist) {
		int sizeAdd = DVDlist.size();
		if(this.qtyOrdered+sizeAdd<=MAX_NUMBER_ORDERED&&this.nbOrdered<MAX_LIMIT_ORDERED){
			while(!DVDlist.isEmpty()){
				DigitalVideoDisc obj = DVDlist.get(0);
				itemsOrdered.add(obj);
				DVDlist.remove(0);
			}
			this.qtyOrdered = itemsOrdered.size();
		} else System.out.println("The order is full");
	}

	// Add double at once
	public void addDigitalVideoDisc(DigitalVideoDisc disc1, DigitalVideoDisc disc2) {
		if(MAX_NUMBER_ORDERED-this.qtyOrdered<=2){
			this.itemsOrdered.add(disc1);
			this.itemsOrdered.add(disc2);
			this.qtyOrdered+=2;
		}
		else if(MAX_NUMBER_ORDERED-this.qtyOrdered==1){
			System.out.println("One item annot be add");
		} else System.out.println("The order is full");
	}

	// Remove existed ordered
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered != 0) {
			itemsOrdered = itemsOrdered.stream().filter(d -> d != disc).collect(Collectors.toList());
			qtyOrdered = itemsOrdered.size();
			System.out.format("The disc \"%s\" has been removed\n", disc.getTitle());
		} else {
			System.out.println("Order is empty. Please add!");
		}
	}

	// Return total cost of order
	public double totalCost() {
		return itemsOrdered.stream().mapToDouble(d -> d.getCost()).sum();
	}
	
	public DigitalVideoDisc getLuckyItem() {
		int itemIndex = (int) (Math.random()*qtyOrdered);
		DigitalVideoDisc Item = itemsOrdered.get(itemIndex);
		Item.setCost(0);
		return Item;
	}

	public void printOrder() {
		System.out.println("++++++++ ORDER LIST +++++++++");
		System.out.println("Date: "+this.dateOrdered);
		int i =0;
		while(i<this.itemsOrdered.size()){
			DigitalVideoDisc item = this.itemsOrdered.get(i);
			System.out.println("DVD: "+item.getTitle()+" "+item.getCategory()+" "+item.getDirectory()
								+" "+item.getLength()+" "+item.getCost());
			i++;
		}
		System.out.print("\nTotal Cost: "+totalCost()+"\n");
		System.out.println("++++++++++++++++++++++++++++++");
	}
	
	public void printOrd_W_LuckyItem() {
		System.out.println("++++++++ ORDER LIST +++++++++");
		System.out.println("Date: "+this.dateOrdered);
		int i =0;
		while(i<this.itemsOrdered.size()){
			DigitalVideoDisc item = this.itemsOrdered.get(i);
			System.out.println("DVD: "+item.getTitle()+" "+item.getCategory()+" "+item.getDirectory()
								+" "+item.getLength()+" "+item.getCost());
			i++;
		}
		DigitalVideoDisc lucky = getLuckyItem();
		System.out.println("The lucky item is: "+lucky.getTitle()+" "+lucky.getCategory()+" "+lucky.getDirectory()
								+" "+lucky.getLength()+" "+lucky.getCost());
		
		System.out.print("\nTotal Cost is: "+totalCost()+"\n");
		System.out.println("++++++++++++++++++++++++++++++");
	}
}
