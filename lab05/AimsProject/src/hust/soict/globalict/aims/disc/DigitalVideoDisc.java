package hust.soict.globalict.aims.disc;

public class DigitalVideoDisc {
	private String title;
	private String category;
	private String directory;
	private int length;
	private float cost;

	public DigitalVideoDisc() {
	}

	public DigitalVideoDisc(String title) {
		this.title = title;
	}

	public DigitalVideoDisc(String title, String category) {
		this.title = title;
		this.category = category;
	}

	public DigitalVideoDisc(String title, String category, String directory) {
		this.title = title;
		this.category = category;
		this.directory = directory;
	}

	public DigitalVideoDisc(String title, String category, String directory, int length, float cost) {
		this.title = title;
		this.category = category;
		this.directory = directory;
		this.length = length;
		this.cost = cost;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	@Override
	public String toString() {
		return "DigitalVideoDisc [title=" + title + ", category=" + category + ", directory=" + directory + ", length="
				+ length + ", cost=" + cost + "]";
	}
	
	public boolean search(String title) {
		title = title.toLowerCase();
		String[] keys = title.split(" ");
		String discName = this.title.toLowerCase();
		
		for (int i = 0; i < keys.length; i++) {
			if (discName.indexOf(keys[i]) == -1)
				return false;
		}
		return true;
	}
}
