// package AimsProject;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class Aims {
    public static void main(String[] args) {
		Order anOrder = new Order();
		anOrder.setNbOrdered();

		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 86, 19.95f);
		anOrder.addDigitalVideoDisc(dvd1);

		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124, 24.95f);
		anOrder.addDigitalVideoDisc(dvd2);

		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Alladin", "Animation", "John Musker", 90, 18.99f);
		anOrder.addDigitalVideoDisc(dvd3);
		anOrder.addDigitalVideoDisc(dvd3);
		anOrder.removeDigitalVideoDisc(dvd3);
		anOrder.addDigitalVideoDisc(dvd3);
		anOrder.printOrd();

		// Add list of Order
		Order order2 = new Order();
		List<DigitalVideoDisc> orderList = new ArrayList<>();
		orderList.add(new DigitalVideoDisc("The Lion Kingz", "Animation", "Roger Allers", 86, 29.95f));
		orderList.add(new DigitalVideoDisc("Star Warsz", "Science Fiction", "George Lucas", 124, 21.95f));
		orderList.add(new DigitalVideoDisc("Alladinz", "Animation", "John Musker", 90, 98.99f));
		order2.addDigitalVideoDisc(orderList);
		order2.printOrd();

		Order order4 = new Order();
		order4.addDigitalVideoDisc(new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 86, 19.95f),
				new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124, 24.95f));
		order4.printOrder();
		
		System.exit(1);
    }
}