import java.time.LocalDate;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class DateTest {
    public static void main(String[] args) {
        MyDate myDate = new MyDate();
		myDate.accept();
        myDate.print();
        
        myDate.acceptStr();
        myDate.printStr();

        List<LocalDate> list = new ArrayList<>();

		list.add(LocalDate.of(2020, 8, 14));
		list.add(LocalDate.of(1972, 11, 30));
		list.add(LocalDate.now());
		list.add(LocalDate.of(1999, 5, 14));

		System.out.println("Before sorting");
		list.forEach(System.out::println);
		System.out.println("After sorting");
		list = DateUtils.sortDates(list);
		list.forEach(System.out::println);
    }
}