import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class DateUtils {
	public static void dateCompare(LocalDate date1, LocalDate date2) {
		if (date1.compareTo(date2) > 0) 
			System.out.println(date1.toString() + " is later than " + date2.toString());
		else 
			System.out.println(date1.toString() + " is earlier than " + date2.toString());
	}
	
	public static List<LocalDate> sortDates(List<LocalDate> list) {
		return list.stream().sorted(LocalDate::compareTo).collect(Collectors.toList());
	}
}