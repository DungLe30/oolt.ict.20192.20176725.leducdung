package hust.soict.globalict.aims.media;

import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;

import hust.soict.globalict.aims.media.Disc;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.media.Playable;

public class CompactDisc extends Disc implements Playable {
	private String artist;
	private int length;
	private List<Track> tracks = new ArrayList<>();

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	
	public void addTrack(Track track) {
		if(this.tracks.contains(track)) {
			System.out.println("The track is already existed in the CD.");
			return;
		}
		this.tracks.add(track);
		return;
	}
	
	public void removeTrack(Track track) {
		if(this.tracks.contains(track)) {
			this.tracks = this.tracks.stream().filter(t->t!=track).collect(Collectors.toList());
			System.out.println("Track has been removed.");
			return;
		}
		System.out.println("Track is not existed in the CD. ");
		return;
	}
	
	public int getLength() {
		this.length = tracks.stream().mapToInt(t->t.getLength()).sum();
		return this.length;
	}

	public CompactDisc() {
		// TODO Auto-generated constructor stub
	}
	
	public CompactDisc(String title) {
		super(title);
	}

	public CompactDisc(String title, String category) {
		super(title, category);
	}

	public CompactDisc(String title, String category, String directory) {
		super(title, category, directory);
	}

	public CompactDisc(String title, String category, String directory, float cost) {
		super(title, category, directory, cost);
	}
	
	public CompactDisc(int id, String title, String category, String directory, int length, float cost) {
		super(id, title, category, directory, length, cost);
	}
	
	public CompactDisc(String title, String artist, List<Track> track) {
		super(title);
		this.artist = artist;
		this.tracks = track;
		this.length = this.getLength();
	}
	
	public CompactDisc(String title, String category, String artist, List<Track> track, float cost) {
		super(title, category, cost);
		this.artist = artist;
		this.tracks = track;
		this.length = this.getLength();
	}
	
	public CompactDisc(int id, String title, String artist, List<Track> track, float cost) {
		super(id, title, cost);
		this.artist = artist;
		this.tracks = track;
		this.length = this.getLength();
	}
	
	@Override
	public String toString() {
		return "CompactDisc [ title=" + getTitle() + "artist=" + artist + ", length=" + length + ", tracks=" + tracks
				+ ", cost = $" + getCost() + "]";
	}
	
	public boolean search(String title) {
		title = title.toLowerCase();
		String[] keys = title.split(" ");
		String discName = getTitle();
		discName = discName.toLowerCase();		
		for (int i = 0; i < keys.length; i++) {
			if (discName.indexOf(keys[i]) == -1)
				return false;
		}
		return true;
	}
	
	public void play() {
		this.tracks.forEach(track -> {
			track.play();
			System.out.println();
		});
	}
}
